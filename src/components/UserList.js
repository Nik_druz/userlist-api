import React from 'react';
import style from './styleComponent.module.css'
import {withStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import axios from "axios"

const styles = (theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(2),
    },
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
});

const DialogTitle = withStyles(styles)((props) => {
    const {children, classes, onClose, ...other} = props;
    return (
        <MuiDialogTitle disableTypography className={classes.root} {...other}>
            <Typography component="div" variant="h6">{children}</Typography>
            {onClose ? (
                <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
                    <CloseIcon/>
                </IconButton>
            ) : null}
        </MuiDialogTitle>
    );
});

const DialogContent = withStyles((theme) => ({
    root: {
        textAlign: "center",
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);




// eslint-disable-next-line import/no-anonymous-default-export
export default (props) => {

    function createUser() {
        axios.post(
            "https://reqres.in/api/users")
            .then(result => {
                console.log(result)
                console.log("done")
            }).catch(e => {
                console.log(e)
        })
    }

    function updateUser() {
        axios.put(
            `https://reqres.in/api/users/${user.id}`)
            .then(result => {
                console.log(result)
                console.log("done")
            }).catch(e => {
                console.log(e)
        })
    }

    function deleteUser() {
        axios.delete(
            `https://reqres.in/api/users/${user.id}`)
            .then(result => {
                console.log(result)
                console.log("done")
            }).catch(e => {
                console.log(e)
        })
    }

    const [open, setOpen] = React.useState(false);
    const [user, setUser] = React.useState({})

    const handleClose = () => {
        setOpen(false);
    };

    return (
        <React.Fragment>
            <div className={style.cardsList}>
                {
                    props.data.map((user) =>
                        <div key={user.id} className={style.userCard}
                             onClick={() => {
                                 setOpen(true);
                                 setUser(user)
                             }}
                             style={{backgroundImage: "url(" + user.avatar + ")"}}>
                            <p>
                                {user.first_name}
                            </p>
                        </div>
                    )
                }
            </div>
            <div className={style.newUserCard}>
                <div className={style.newUserItem} onClick={createUser}>
                    Cоздать нового
                </div>
            </div>

            <Dialog onClose={handleClose} aria-labelledby="customized-dialog-title" open={open}>
                <DialogTitle id="customized-dialog-title" onClose={handleClose}>
                    {user.first_name}
                </DialogTitle>
                <DialogContent dividers>
                    <img src={user.avatar} alt="user_image"/>
                    <form action="" className={style.form}>
                        <p>First Name</p>
                        <input type="text"
                               defaultValue={user.first_name}
                        />
                        <p>Last Name</p>
                        <input type="text"
                               defaultValue={user.last_name}
                        />
                        <p>Email</p>
                        <input type="text"
                               defaultValue={user.email}
                        />
                    </form>
                </DialogContent>
                <DialogActions className={style.MuiDialogActions}>
                    <Button onClick={()=>{
                            deleteUser()
                            setOpen(false)
                        }}
                        color="primary">
                        Удалить
                    </Button>
                    <Button onClick={()=>{
                        updateUser()
                        setOpen(false)
                    }} color="primary">
                        Сохранить изменения
                    </Button>
                </DialogActions>
            </Dialog>

        </React.Fragment>
    )
}