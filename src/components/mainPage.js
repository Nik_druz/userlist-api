import React from 'react';
import { Get } from 'react-axios'
import UserList from "./UserList";

export function MainPage (props){
    return(
        <div>
            <Get url="https://reqres.in/api/users" params={{page: props.page}}>
                {(error, response, isLoading, makeRequest, axios) => {
                    if(error) {
                        return (<div>Something bad happened: {error.message} <button onClick={() => makeRequest({ params: { reload: true } })}>Retry</button></div>)
                    }
                    else if(isLoading) {
                        return (<div>Loading...</div>)
                    }
                    else if(response !== null) {
                        return ( <UserList data={response.data.data}/>)
                    }
                    return (<div>Default message before request is made.</div>)
                }}
            </Get>
        </div>
    )
}