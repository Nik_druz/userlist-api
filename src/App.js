import React from 'react';
import {MainPage} from './components/mainPage'
import Container from "@material-ui/core/Container";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            marginTop: theme.spacing(2),

        },
    },
}));

export default () =>{
    const [page, setPage] = React.useState(1);
    const [totalPage, setTotalPage]  = React.useState(1);

    (async function () {
        const res = await fetch("https://reqres.in/api/users/");
        const json = await res.json();
        setTotalPage(json.total_pages);
    })();

    const classes = useStyles();

    const handleChange = (event, value) => {
        setPage(value);
    };
    return(
        <React.Fragment>
            <Container maxWidth="lg" className={classes.root}>
                <Typography component="div" style={{ backgroundColor: '#edeef0', height: 'calc(100% - 60px)',paddingTop: '30px' , paddingBottom: "50px"}}>
                    <MainPage  page={page}/>
                </Typography>
                <Pagination style={{ display: "flex", justifyContent: "center"}} count={totalPage} color="primary"  page={page} onChange={handleChange}/>
            </Container>
        </React.Fragment>
    )
}